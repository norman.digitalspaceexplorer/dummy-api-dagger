package com.example.dummyapidagger.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.dummyapidagger.api.NetworkComments
import com.example.dummyapidagger.entities.CommentsEntity
import com.example.dummyapidagger.models.CommentsModel
import com.example.dummyapidagger.objects.CommentsContent
import com.example.dummyapidagger.room.DummyAPIDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CommentsRepository(private val dummyAPIDatabase: DummyAPIDatabase) {

    var postComments: MutableLiveData<List<CommentsModel>>? = null

    val allPostComments: LiveData<List<CommentsModel>> = Transformations.map(dummyAPIDatabase.commentsDao.getAllPostComments()) {
        it.asCommentsModel()
    }

    suspend fun deleteAllComments() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.commentsDao.deleteAll()
        }
    }

    suspend fun getAllDomainComments(postID: String, page: Int) {
        withContext(Dispatchers.IO) {
            val comments = NetworkComments.api.getCommentsAsync(postID = postID, page = page).await()
            dummyAPIDatabase.commentsDao.insertAll(commentsEntity = comments.data.asCommentsEntity(postID = postID))
        }
    }

}

fun List<CommentsEntity>.asCommentsModel() : List<CommentsModel> {
    return map {
        CommentsModel(
            it.id, it.userID,
            it.lastName, it.firstName, it.email,
            it.title, it.pictureLink, it.commentID,
            it.comment, it.publishDate, it.postsID
        )
    }
}

fun List<CommentsContent>.asCommentsEntity(postID: String) : List<CommentsEntity> {
    return map {
        CommentsEntity(
            id = 0, userID = it.owner.id,
            lastName = it.owner.lastName, firstName = it.owner.firstName, email = it.owner.email,
            title = it.owner.title, pictureLink = it.owner.picture, commentID = it.id,
            comment = it.message, publishDate = it.publishDate, postsID = postID
        )
    }
}