package com.example.dummyapidagger.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.dummyapidagger.api.NetworkUsers
import com.example.dummyapidagger.entities.UsersEntity
import com.example.dummyapidagger.models.UsersModel
import com.example.dummyapidagger.objects.UsersData
import com.example.dummyapidagger.room.DummyAPIDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UsersRepository(private val dummyAPIDatabase: DummyAPIDatabase) {

    val allUsers: LiveData<List<UsersModel>> = Transformations.map(dummyAPIDatabase.usersDao.getAllUsers()) {
        it.asUsersModel()
    }

    suspend fun deleteAllUsers() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.usersDao.deleteAll()
        }
    }

    suspend fun getDomainUsers(page: Int) {
        withContext(Dispatchers.IO) {
            val usersList = NetworkUsers.api.getUsersAsync(page = page).await()
            dummyAPIDatabase.usersDao.insertAll(usersEntity = usersList.data.asUsersDomainModel())
        }
    }

}

fun List<UsersEntity>.asUsersModel() : List<UsersModel> {
    return map {
        UsersModel(
            it.id, it.userID, it.lastName,
            it.firstName, it.email, it.title, it.pictureLink
        )
    }
}

fun List<UsersData>.asUsersDomainModel() : List<UsersEntity> {
    return map {
        UsersEntity(
            id = 0, it.id, it.lastName,
            it.firstName, it.email, it.title, it.picture
        )
    }
}
