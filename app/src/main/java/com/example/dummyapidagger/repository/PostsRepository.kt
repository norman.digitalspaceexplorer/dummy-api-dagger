package com.example.dummyapidagger.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.dummyapidagger.api.NetworkPosts
import com.example.dummyapidagger.entities.PostsEntity
import com.example.dummyapidagger.entities.TIDEntity
import com.example.dummyapidagger.models.PostsModel
import com.example.dummyapidagger.objects.PostsData
import com.example.dummyapidagger.room.DummyAPIDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PostsRepository(private val dummyAPIDatabase: DummyAPIDatabase) {

    val allPosts: LiveData<List<PostsModel>> = Transformations.map(dummyAPIDatabase.postsDao.getAllPosts()) {
        it.asPostsModel()
    }
    private var userPosts: LiveData<List<PostsEntity>>? = null

    suspend fun deleteAllPosts() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.postsDao.deleteAll()
        }
    }

    suspend fun getDomainPosts(page: Int) {
        withContext(Dispatchers.IO) {
            val posts = NetworkPosts.api.getPostsAsync(page = page).await()
            dummyAPIDatabase.tidDao.insertAll(tidEntity = posts.data.asTIDEntity())
            dummyAPIDatabase.postsDao.insertAll(postsEntity = posts.data.asPostEntity())
        }
    }
}

fun List<PostsEntity>.asPostsModel() : List<PostsModel> {
    return map {
        PostsModel(
            it.id, it.userID, it.lastName,
            it.firstName, it.email, it.title,
            it.pictureLink, it.postID, it.postImage,
            it.postContent, it.publishDate,
            it.links, it.likes
        )
    }
}

fun List<PostsData>.asTIDEntity() : List<TIDEntity> {
    val tidEntities : MutableList<TIDEntity> = mutableListOf()
    map {
        it.tags.map { tag ->
            tidEntities.add(TIDEntity(id = 0, postID = it.postID, tagNames = tag))
        }
    }
    return tidEntities
}

fun List<PostsData>.asPostEntity() : List<PostsEntity> {
    return map {
        PostsEntity(
            id = 0, userID = it.owner.id, lastName = it.owner.lastName, firstName = it.owner.firstName,
            email = it.owner.email, title = it.owner.title, pictureLink = it.owner.picture,
            postID = it.postID, postImage = it.postImage, postContent = it.text,
            publishDate = it.postPublishDate, links = it.link, likes = it.likes
        )
    }
}