package com.example.dummyapidagger.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.dummyapidagger.api.NetworkPosts
import com.example.dummyapidagger.api.NetworkUserProfile
import com.example.dummyapidagger.entities.UserPostsEntity
import com.example.dummyapidagger.entities.UserProfileEntity
import com.example.dummyapidagger.entities.UserTIDEntity
import com.example.dummyapidagger.models.TIDModel
import com.example.dummyapidagger.models.UserPostsModel
import com.example.dummyapidagger.models.UserProfileModel
import com.example.dummyapidagger.objects.Location
import com.example.dummyapidagger.objects.PostsData
import com.example.dummyapidagger.objects.UserProfile
import com.example.dummyapidagger.room.DummyAPIDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserProfileRepository(private val dummyAPIDatabase: DummyAPIDatabase) {

    val allUserProfile : LiveData<List<UserProfileModel>> = Transformations.map(dummyAPIDatabase.userProfileDao.getAllUserProfile()) {
        it.asUserProfileModel()
    }
    val allUserPosts : LiveData<List<UserPostsModel>> = Transformations.map(dummyAPIDatabase.userPostsDao.getAllUserPosts()) {
        it.asUserPostsModel()
    }
    val allUserPostsTID : LiveData<List<TIDModel>> = Transformations.map(dummyAPIDatabase.userTIDDao.getAllPostTags()) {
        it.asUserTIDModel()
    }

    suspend fun deleteAllUserProfile() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.userProfileDao.deleteAll()
        }
    }

    suspend fun deleteAllUserPosts() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.userPostsDao.deleteAll()
        }
    }

    suspend fun deleteAllUserPostsTIDs() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.userTIDDao.deleteAll()
        }
    }

    suspend fun getDomainUserProfile(userID: String) {
        withContext(Dispatchers.IO) {
            val userProfile = NetworkUserProfile.api.getUserProfileAsync(userID = userID).await()
            dummyAPIDatabase.userProfileDao.insertAll(userProfileEntity = userProfile.asUserProfileEntity())
        }
    }

    suspend fun getDomainUserPosts(userID: String, page: Int) {
        withContext(Dispatchers.IO) {
            val userPosts = NetworkPosts.api.getUserPostsAsync(userID = userID, page = page).await()
            dummyAPIDatabase.userTIDDao.insertAll(userTIDEntity = userPosts.data.asUserTIDEntity())
            dummyAPIDatabase.userPostsDao.insertAll(userPostsEntity = userPosts.data.asUserPostEntity())
        }
    }

}

fun List<UserProfileEntity>.asUserProfileModel() : List<UserProfileModel> {
    return map {
        UserProfileModel(
            userID = it.userID, phone = it.phone, lastName = it.lastName, firstName = it.firstName,
            location = Location(state = it.locationState, street = it.locationStreet,
                city = it.locationCity, timezone = it.locationTimeZone, country = it.locationCountry
            ), email = it.email, gender = it.gender, title = it.title,
            registeredDate = it.registeredDate, pictureLink = it.pictureLink, dateOfBirth = it.dateOfBirth
        )
    }
}

fun UserProfile.asUserProfileEntity() : List<UserProfileEntity> {
    val tempEntity : MutableList<UserProfileEntity> = mutableListOf()
    tempEntity.add(
        UserProfileEntity(
            id = 0, userID = userID, phone = phone, lastName = lastName,
            firstName = firstName, locationState = location.state, locationStreet = location.street,
            locationCity = location.city, locationTimeZone = location.timezone, locationCountry = location.country,
            email = email, gender = gender, title = title,
            registeredDate = registeredDate, pictureLink = pictureLink, dateOfBirth = dateOfBirth
        )
    )
    return tempEntity
}

fun List<UserPostsEntity>.asUserPostsModel() : List<UserPostsModel> {
    return map {
        UserPostsModel(
            id = it.id, userID = it.userID, lastName = it.lastName, firstName = it.firstName, email = it.email,
            title = it.title, pictureLink = it.pictureLink, postID = it.postID, postImage = it.postImage,
            postContent = it.postContent, publishDate = it.publishDate, links = it.links, likes = it.likes
        )
    }
}

fun List<PostsData>.asUserPostEntity() : List<UserPostsEntity> {
    return map {
        UserPostsEntity(
            id = 0, userID = it.owner.id, lastName = it.owner.lastName, firstName = it.owner.firstName, email = it.owner.email,
            title = it.owner.title, pictureLink = it.owner.picture, postID = it.postID, postImage = it.postImage,
            postContent = it.text, publishDate = it.postPublishDate, links = it.link, likes = it.likes
        )
    }
}

fun List<UserTIDEntity>.asUserTIDModel() : List<TIDModel> {
    return map { TIDModel(it.id, it.postID, it.tagNames) }
}

fun List<PostsData>.asUserTIDEntity() : List<UserTIDEntity> {
    val userTIDEntities : MutableList<UserTIDEntity> = mutableListOf()
    map {
        it.tags.map { tag ->
            userTIDEntities.add(UserTIDEntity(id = 0, postID = it.postID, tagNames = tag))
        }
    }
    return userTIDEntities
}