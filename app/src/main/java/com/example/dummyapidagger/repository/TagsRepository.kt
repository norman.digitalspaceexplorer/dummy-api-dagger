package com.example.dummyapidagger.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.dummyapidagger.api.NetworkTags
import com.example.dummyapidagger.entities.TagsEntity
import com.example.dummyapidagger.entities.TagsPostsEntity
import com.example.dummyapidagger.entities.TagsPostsTIDEntity
import com.example.dummyapidagger.models.PostsModel
import com.example.dummyapidagger.models.TIDModel
import com.example.dummyapidagger.models.TagsModel
import com.example.dummyapidagger.objects.PostsData
import com.example.dummyapidagger.room.DummyAPIDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TagsRepository(private val dummyAPIDatabase: DummyAPIDatabase) {

    val allTags: LiveData<List<TagsModel>> = Transformations.map(dummyAPIDatabase.tagsDao.getAllTags()) {
        it.asTagsModel()
    }
    val allTagsPosts: LiveData<List<PostsModel>> = Transformations.map(dummyAPIDatabase.tagsPostsDao.getAllTagsPosts()) {
        it.asPostsModel()
    }
    val allTagsPostsTID: LiveData<List<TIDModel>> = Transformations.map(dummyAPIDatabase.tagsPostsTIDDao.getAllPostTags()) {
        it.asTagsPostsTIDModel()
    }

    suspend fun deleteAllTag() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.tagsDao.deleteAll()
        }
    }

    suspend fun deleteAllTagPosts() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.tagsPostsDao.deleteAll()
        }
    }

    suspend fun deleteAllTagPostsTID() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.tagsPostsTIDDao.deleteAll()
        }
    }

    suspend fun getAllDomainTags(page: Int) {
        withContext(Dispatchers.IO) {
            val tags = NetworkTags.api.getTagsAsync(page = page).await()
            dummyAPIDatabase.tagsDao.insertAll(tagsEntity = tags.data.asTagsEntity())
        }
    }

    suspend fun getAllDomainTagsPosts(postTag: String, page: Int) {
        withContext(Dispatchers.IO) {
            val posts = NetworkTags.api.getPostsAsync(postTag = postTag, page = page).await()
            dummyAPIDatabase.tagsPostsTIDDao.insertAll(tagsPostsTIDEntity = posts.data.asTagsPostsTIDEntity())
            dummyAPIDatabase.tagsPostsDao.insertAll(tagsPostsEntity = posts.data.asTagPostsEntity())
        }
    }

}

fun List<TagsEntity>.asTagsModel() : List<TagsModel> {
    return map { TagsModel(it.id, it.tags) }
}

fun List<String>.asTagsEntity() : List<TagsEntity> {
    return map { TagsEntity(id = 0, tags = it) }
}

fun List<TagsPostsEntity>.asPostsModel() : List<PostsModel> {
    return map {
        PostsModel(
            id = it.id, userID = it.userID, lastName = it.lastName, firstName = it.firstName,
            email = it.email, title = it.title, pictureLink = it.pictureLink,
            postID = it.postID, postImage = it.postImage, postContent = it.postContent,
            publishDate = it.publishDate, links = it.links, likes = it.likes
        )
    }
}

fun List<PostsData>.asTagPostsEntity() : List<TagsPostsEntity> {
    return map {
        TagsPostsEntity(
            id = 0, userID = it.owner.id, lastName = it.owner.lastName, firstName = it.owner.firstName,
            email = it.owner.email, title = it.owner.title, pictureLink = it.owner.picture,
            postID = it.postID, postImage = it.postImage, postContent = it.text,
            publishDate = it.postPublishDate, links = it.link, likes = it.likes
        )
    }
}

fun List<TagsPostsTIDEntity>.asTagsPostsTIDModel() : List<TIDModel> {
    return map { TIDModel(it.id, it.postID, it.tagNames) }
}

fun List<PostsData>.asTagsPostsTIDEntity() : List<TagsPostsTIDEntity> {
    val tagsPostsTIDEntities : MutableList<TagsPostsTIDEntity> = mutableListOf()
    map {
        it.tags.map { tag ->
            tagsPostsTIDEntities.add(TagsPostsTIDEntity(id = 0, postID = it.postID, tagNames = tag))
        }
    }
    return tagsPostsTIDEntities
}