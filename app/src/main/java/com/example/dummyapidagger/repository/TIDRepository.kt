package com.example.dummyapidagger.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.dummyapidagger.entities.TIDEntity
import com.example.dummyapidagger.models.TIDModel
import com.example.dummyapidagger.room.DummyAPIDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TIDRepository(private val dummyAPIDatabase: DummyAPIDatabase) {

    val allPostTags: LiveData<List<TIDModel>> = Transformations.map(dummyAPIDatabase.tidDao.getAllPostTags()) {
        it.asTIDModel()
    }

    suspend fun deleteAllTagID() {
        withContext(Dispatchers.IO) {
            dummyAPIDatabase.tidDao.deleteAll()
        }
    }

}

fun List<TIDEntity>.asTIDModel() : List<TIDModel> {
    return map {
        TIDModel(
            it.id,
            it.postID,
            it.tagNames
        )
    }
}