package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.UsersEntity

@Dao
interface UsersDao {

    @Insert
    fun insert(usersEntity: UsersEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(usersEntity: List<UsersEntity>)

    @Update
    fun update(usersEntity: UsersEntity)

    @Delete
    fun delete(usersEntity: UsersEntity)

    @Query("DELETE FROM VEN_TBL_USERS")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_USERS")
    fun getAllUsers() : LiveData<List<UsersEntity>>

}

