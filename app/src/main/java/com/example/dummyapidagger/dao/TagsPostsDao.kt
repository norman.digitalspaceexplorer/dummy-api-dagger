package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.TagsPostsEntity

@Dao
interface TagsPostsDao {

    @Insert
    fun insert(tagsPostsEntity: TagsPostsEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(tagsPostsEntity: List<TagsPostsEntity>)

    @Update
    fun update(tagsPostsEntity: TagsPostsEntity)

    @Delete
    fun delete(tagsPostsEntity: TagsPostsEntity)

    @Query("DELETE FROM VEN_TBL_TAGS_POSTS")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_TAGS_POSTS")
    fun getAllTagsPosts() : LiveData<List<TagsPostsEntity>>


}