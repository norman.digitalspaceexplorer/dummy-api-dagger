package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.TIDEntity

@Dao
interface TIDDao {

    @Insert
    fun insert(tidEntity: TIDEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(tidEntity: List<TIDEntity>)

    @Update
    fun update(tidEntity: TIDEntity)

    @Delete
    fun delete(tidEntity: TIDEntity)

    @Query("DELETE FROM VEN_TBL_TID")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_TID WHERE post_id = :postID")
    fun getPostTags(postID: String) : LiveData<List<TIDEntity>>

    @Query("SELECT * FROM VEN_TBL_TID")
    fun getAllPostTags() : LiveData<List<TIDEntity>>

}