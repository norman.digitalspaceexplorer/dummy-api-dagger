package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.TagsPostsTIDEntity

@Dao
interface TagsPostsTIDDao {

    @Insert
    fun insert(tagsPostsTIDEntity: TagsPostsTIDEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(tagsPostsTIDEntity: List<TagsPostsTIDEntity>)

    @Update
    fun update(tagsPostsTIDEntity: TagsPostsTIDEntity)

    @Delete
    fun delete(tagsPostsTIDEntity: TagsPostsTIDEntity)

    @Query("DELETE FROM VEN_TBL_TAGS_TID")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_TAGS_TID")
    fun getAllPostTags() : LiveData<List<TagsPostsTIDEntity>>

}