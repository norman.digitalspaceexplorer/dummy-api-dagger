package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.UserTIDEntity

@Dao
interface UserTIDDao {

    @Insert
    fun insert(userTIDEntity: UserTIDEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(userTIDEntity: List<UserTIDEntity>)

    @Update
    fun update(userTIDEntity: UserTIDEntity)

    @Delete
    fun delete(userTIDEntity: UserTIDEntity)

    @Query("DELETE FROM VEN_TBL_USER_TID")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_USER_TID WHERE post_id = :postID")
    fun getPostTags(postID: String) : LiveData<List<UserTIDEntity>>

    @Query("SELECT * FROM VEN_TBL_USER_TID")
    fun getAllPostTags() : LiveData<List<UserTIDEntity>>

}