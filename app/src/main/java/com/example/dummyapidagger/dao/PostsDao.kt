package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.PostsEntity

@Dao
interface PostsDao {

    @Insert
    fun insert(postsEntity: PostsEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(postsEntity: List<PostsEntity>)

    @Update
    fun update(postsEntity: PostsEntity)

    @Delete
    fun delete(postsEntity: PostsEntity)

    @Query("DELETE FROM VEN_TBL_POSTS")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_POSTS")
    fun getAllPosts() : LiveData<List<PostsEntity>>

    @Query("SELECT * FROM VEN_TBL_POSTS WHERE user_id = :userID")
    fun getUserPosts(userID: String) : LiveData<List<PostsEntity>>

}