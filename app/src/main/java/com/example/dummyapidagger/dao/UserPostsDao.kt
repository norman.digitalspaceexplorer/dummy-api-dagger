package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.UserPostsEntity

@Dao
interface UserPostsDao {

    @Insert
    fun insert(userPostsEntity: UserPostsEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(userPostsEntity: List<UserPostsEntity>)

    @Update
    fun update(userPostsEntity: UserPostsEntity)

    @Delete
    fun delete(userPostsEntity: UserPostsEntity)

    @Query("DELETE FROM VEN_TBL_USER_POSTS")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_USER_POSTS")
    fun getAllUserPosts() : LiveData<List<UserPostsEntity>>

}