package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.CommentsEntity

@Dao
interface CommentsDao {

    @Insert
    fun insert(commentsEntity: CommentsEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(commentsEntity: List<CommentsEntity>)

    @Update
    fun update(commentsEntity: CommentsEntity)

    @Delete
    fun delete(commentsEntity: CommentsEntity)

    @Query("DELETE FROM VEN_TBL_COMMENTS")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_COMMENTS WHERE posts_id = :postID")
    fun getPostComments(postID: String) : LiveData<List<CommentsEntity>>

    @Query("SELECT * FROM VEN_TBL_COMMENTS")
    fun getAllPostComments() : LiveData<List<CommentsEntity>>
}