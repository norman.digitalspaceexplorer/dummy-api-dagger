package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.TagsEntity

@Dao
interface TagsDao {

    @Insert
    fun insert(tagsEntity: TagsEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(tagsEntity: List<TagsEntity>)

    @Update
    fun update(tagsEntity: TagsEntity)

    @Delete
    fun delete(tagsEntity: TagsEntity)

    @Query("DELETE FROM VEN_TBL_TAGS")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_TAGS")
    fun getAllTags() : LiveData<List<TagsEntity>>

}