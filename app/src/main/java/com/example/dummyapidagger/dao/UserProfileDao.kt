package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.UserProfileEntity

@Dao
interface UserProfileDao {

    @Insert
    fun insert(userProfileEntity: UserProfileEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(userProfileEntity: List<UserProfileEntity>)

    @Update
    fun update(userProfileEntity: UserProfileEntity)

    @Delete
    fun delete(userProfileEntity: UserProfileEntity)

    @Query("DELETE FROM VEN_TBL_USER_PROFILE")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_USER_PROFILE")
    fun getAllUserProfile() : LiveData<List<UserProfileEntity>>

    @Query("SELECT * FROM VEN_TBL_USER_PROFILE WHERE user_id = :userID")
    fun getUserProfile(userID: String) : LiveData<UserProfileEntity>

}