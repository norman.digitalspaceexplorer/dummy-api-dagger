package com.example.dummyapidagger.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.dummyapidagger.entities.ProfileEntity

@Dao
interface ProfileDao {

    @Insert
    fun insert(profileEntity: ProfileEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(profileEntity: List<ProfileEntity>)

    @Update
    fun update(profileEntity: ProfileEntity)

    @Delete
    fun delete(profileEntity: ProfileEntity)

    @Query("DELETE FROM VEN_TBL_PROFILE")
    fun deleteAll()

    @Query("SELECT * FROM VEN_TBL_PROFILE WHERE user_id = :userID LIMIT 1")
    fun getUserProfile(userID: String) : LiveData<ProfileEntity>

}