package com.example.dummyapidagger.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.dummyapidagger.R
import com.example.dummyapidagger.activities.UserProfile
import com.example.dummyapidagger.adapters.OnUserClick
import com.example.dummyapidagger.adapters.UsersAdapter
import com.example.dummyapidagger.databinding.FragmentUsersListBinding
import com.example.dummyapidagger.utils.NetworkConfig
import com.example.dummyapidagger.viewmodel.DummyAPIViewModel
import timber.log.Timber

class UsersFragment : Fragment(), NestedScrollView.OnScrollChangeListener {

    private var page: Int = 0
    private lateinit var b: FragmentUsersListBinding

    private val vm : DummyAPIViewModel by lazy {
        val activity = requireNotNull(activity) { "Access DummyAPIViewModel in onViewCreated ..." }
        ViewModelProvider(this, DummyAPIViewModel.Factory(activity.application)).get(DummyAPIViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        b = DataBindingUtil.inflate(inflater, R.layout.fragment_users_list, container, false)
        b.lifecycleOwner = viewLifecycleOwner
        return b.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timber.d("UsersFragment Created ...")

        val usersAdapter = UsersAdapter(OnUserClick {
            startActivity(Intent(activity, UserProfile::class.java).putExtra("userID", it.userID))
        })
        b.recyclerViewUsers.adapter = usersAdapter

        if(NetworkConfig(activity).isNetworkAvailable) {
            vm.deleteAllUsers()
            vm.getAllDomainUsers(page = page)
        }
        vm.liveData.allUsers.observe(viewLifecycleOwner, {
            if (it.isEmpty()) {
                setLoadingVisible(visibility = View.VISIBLE)
                return@observe
            }
            usersAdapter.allUsers = it
            setLoadingVisible(visibility = View.INVISIBLE)
            b.scrollView.setOnScrollChangeListener(this)
            page = page.plus(1)
        })

    }

    private fun setLoadingVisible(visibility: Int) {
        b.lottieAnimationView.visibility = visibility
    }

    override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
        if (scrollY == (v!!.getChildAt(0).measuredHeight-v.measuredHeight)) {
            b.progressLoading.visibility = View.VISIBLE
            if (NetworkConfig(activity).isNetworkAvailable) vm.getAllDomainUsers(page = page)
        }
    }

}