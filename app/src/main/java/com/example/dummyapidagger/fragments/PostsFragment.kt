package com.example.dummyapidagger.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.dummyapidagger.R
import com.example.dummyapidagger.activities.PostsComments
import com.example.dummyapidagger.adapters.OnPostsClick
import com.example.dummyapidagger.adapters.PostsAdapter
import com.example.dummyapidagger.databinding.FragmentPostsListBinding
import com.example.dummyapidagger.interfaces.BottomSheetDialogListener
import com.example.dummyapidagger.models.PostsModel
import com.example.dummyapidagger.utils.NetworkConfig
import com.example.dummyapidagger.viewmodel.DummyAPIViewModel
import timber.log.Timber

class PostsFragment : Fragment(), NestedScrollView.OnScrollChangeListener, BottomSheetDialogListener {

    private var page: Int = 0
    private lateinit var b: FragmentPostsListBinding

    private val vm : DummyAPIViewModel by lazy {
        val activity = requireNotNull(activity) { "Access DummyAPIViewModel in onViewCreated ..." }
        ViewModelProvider(this, DummyAPIViewModel.Factory(activity.application)).get(DummyAPIViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        b = DataBindingUtil.inflate(inflater, R.layout.fragment_posts_list, container, false)
        b.lifecycleOwner = this
        return b.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timber.d("PostsFragment Created ...")

        val postsAdapter = PostsAdapter(
            defaultOwner = "Posts",
            onPostsClick = OnPostsClick { onFragmentShow(postsModel = it) },
            vm = vm, lifeCycle = viewLifecycleOwner
        )
        b.recyclerViewPosts.adapter = postsAdapter

        if (NetworkConfig(activity).isNetworkAvailable) {
            vm.deleteAllPosts()
            vm.deleteAllPostTags()
            vm.getAllDomainPosts(page = page)
        }
        vm.liveData.allPosts.observe(viewLifecycleOwner, {
            if (it.isEmpty()) {
                setLoadingVisible(visibility = View.VISIBLE)
                return@observe
            }
            postsAdapter.allPosts = it
            setLoadingVisible(visibility = View.INVISIBLE)
            b.scrollView.setOnScrollChangeListener(this)
            page = page.plus(1)
        })

    }

    private fun setLoadingVisible(visibility: Int) {
        b.lottieAnimationView.visibility = visibility
    }

    override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
        if (scrollY == (v!!.getChildAt(0).measuredHeight-v.measuredHeight)) {
            b.progressLoading.visibility = View.VISIBLE
            if (NetworkConfig(activity).isNetworkAvailable) vm.getAllDomainPosts(page = page)
        }
    }

    override fun onFragmentShow(postsModel: PostsModel?) {
        val bundle = Bundle()
        val postsComments = PostsComments()
        bundle.putString("postID", postsModel!!.postID)
        bundle.putString("totalLikes", postsModel.likes.toString())
        postsComments.arguments = bundle
        postsComments.show(activity!!.supportFragmentManager, "PostComments")
    }

}