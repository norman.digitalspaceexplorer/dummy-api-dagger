package com.example.dummyapidagger.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.dummyapidagger.R
import com.example.dummyapidagger.activities.TagsPosts
import com.example.dummyapidagger.adapters.OnTagClick
import com.example.dummyapidagger.adapters.TagsAdapter
import com.example.dummyapidagger.databinding.FragmentTagsListBinding
import com.example.dummyapidagger.utils.NetworkConfig
import com.example.dummyapidagger.viewmodel.DummyAPIViewModel
import timber.log.Timber

class TagsFragment : Fragment() {

    private var page: Int = 0
    private lateinit var b: FragmentTagsListBinding

    private val vm : DummyAPIViewModel by lazy {
        val activity = requireNotNull(activity) { "Access DummyAPIViewModel in onViewCreated ..." }
        ViewModelProvider(this, DummyAPIViewModel.Factory(activity.application)).get(DummyAPIViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        b = DataBindingUtil.inflate(inflater, R.layout.fragment_tags_list, container, false)
        return b.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Timber.d("TagsFragment Created ...")

        val tagsAdapter = TagsAdapter(OnTagClick {
            startActivity(Intent(activity, TagsPosts::class.java).putExtra("PostTag", it.tags))
        })
        val layoutManager = StaggeredGridLayoutManager(3, LinearLayoutManager.VERTICAL)

        b.recyclerViewTags.layoutManager = layoutManager
        b.recyclerViewTags.adapter = tagsAdapter

        if (NetworkConfig(activity).isNetworkAvailable) {
            vm.deleteAllTags()
            vm.getAllDomainTags(page = page)
        }
        vm.liveData.allTags.observe(viewLifecycleOwner, {
            if (it.isEmpty()) {
                setLoadingVisible(visibility = View.VISIBLE)
                return@observe
            }
            tagsAdapter.allTags = it
            setLoadingVisible(visibility = View.INVISIBLE)
        })

    }

    private fun setLoadingVisible(visibility: Int) {
        b.lottieAnimationView.visibility = visibility
    }

}