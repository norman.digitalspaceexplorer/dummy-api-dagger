package com.example.dummyapidagger;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.dummyapidagger.databinding.ActivityMainBinding;
import com.example.dummyapidagger.listeners.BottomNavigationListener;

@SuppressWarnings({ "NonConstantResourceId" })
public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar(binding.toolbar);
        setTitle(getResources().getString(R.string.users_list));

        BottomNavigationListener bottomNavigationListener = new BottomNavigationListener(this, getSupportFragmentManager());
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(bottomNavigationListener);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.invalidateAll();
    }

}