package com.example.dummyapidagger.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.dummyapidagger.dagger.component.DaggerViewModelComponent
import com.example.dummyapidagger.dagger.modules.DatabaseModule
import com.example.dummyapidagger.usecases.LiveDataUseCase
import com.example.dummyapidagger.usecases.RepositoryUseCase
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import javax.inject.Inject

class DummyAPIViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var repository: RepositoryUseCase

    @Inject
    lateinit var liveData: LiveDataUseCase

    init {
        DaggerViewModelComponent.builder()
            .databaseModule(DatabaseModule(application = application))
            .build()
            .inject(viewModel = this)
    }

    fun getAllDomainUsers(page: Int) {
        viewModelScope.launch {
            repository.user.getDomainUsers(page = page)
        }
    }

    fun getAllDomainPosts(page: Int) {
        viewModelScope.launch {
            repository.posts.getDomainPosts(page = page)
        }
    }

    fun getAllDomainTags(page: Int) {
        viewModelScope.launch {
            repository.tags.getAllDomainTags(page = page)
        }
    }

    fun getDomainUserProfile(userID: String) {
        viewModelScope.launch {
            repository.profile.getDomainUserProfile(userID = userID)
        }
    }

    fun getDomainUserPosts(userID: String, page: Int) {
        viewModelScope.launch {
            repository.profile.getDomainUserPosts(userID = userID, page = page)
        }
    }

    fun getAllDomainPostComments(postID: String, page: Int) {
        viewModelScope.launch {
            repository.comments.getAllDomainComments(postID = postID, page = page)
        }
    }

    fun getAllDomainTagPosts(postTag: String, page: Int) {
        viewModelScope.launch {
            repository.tags.getAllDomainTagsPosts(postTag = postTag, page = page)
        }
    }

    fun deleteAllUsers() {
        viewModelScope.launch {
            repository.user.deleteAllUsers()
        }
    }

    fun deleteAllPosts() {
        viewModelScope.launch {
            repository.posts.deleteAllPosts()
        }
    }

    fun deleteAllPostTags() {
        viewModelScope.launch {
            repository.tid.deleteAllTagID()
        }
    }

    fun deleteAllTags() {
        viewModelScope.launch {
            repository.tags.deleteAllTag()
        }
    }

    fun deleteAllUserProfile() {
        viewModelScope.launch {
            repository.profile.deleteAllUserProfile()
        }
    }

    fun deleteAllUserPosts() {
        viewModelScope.launch {
            repository.profile.deleteAllUserPosts()
        }
    }

    fun deleteAllUserPostsTags() {
        viewModelScope.launch {
            repository.profile.deleteAllUserPostsTIDs()
        }
    }

    fun deleteAllUserPostsComments() {
        viewModelScope.launch {
            repository.comments.deleteAllComments()
        }
    }

    fun deleteAllTagPosts() {
        viewModelScope.launch {
            repository.tags.deleteAllTagPosts()
        }
    }

    fun deleteAllTagPostsTID() {
        viewModelScope.launch {
            repository.tags.deleteAllTagPostsTID()
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DummyAPIViewModel::class.java)) {
                return DummyAPIViewModel(application) as T
            }
            throw IllegalArgumentException("Unable to construct ViewModel DummyAPIViewModel::class.java")
        }
    }

}