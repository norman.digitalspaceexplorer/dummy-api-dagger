package com.example.dummyapidagger.models

data class UserPostsModel constructor(
    val id: Int,
    val userID: String,
    val lastName: String,
    val firstName: String,
    val email: String,
    val title: String,
    val pictureLink: String,
    val postID: String,
    val postImage: String,
    val postContent: String,
    val publishDate: String,
    val links: String?,
    val likes: Int
)