package com.example.dummyapidagger.models

data class CommentsModel constructor(
    val id: Int,
    val userID: String,
    val lastName: String,
    val firstName: String,
    val email: String,
    val title: String,
    val pictureLink: String,
    val commentID: String,
    val comment: String,
    val publishDate: String,
    val postsID: String
)