package com.example.dummyapidagger.models

data class TIDModel constructor(
    val id: Int,
    val postID: String,
    val tagNames: String
)