package com.example.dummyapidagger.models

data class TagsModel constructor(
    val id: Int,
    val tags: String
)