package com.example.dummyapidagger.models

data class UsersModel constructor(
    val id: Int,
    val userID: String,
    val lastName: String,
    val firstName: String,
    val email: String,
    val title: String,
    val pictureLink: String
)