package com.example.dummyapidagger.models

data class ProfileModel constructor(
    val id: Int,
    val userID: String,
    val phone: String,
    val lastName: String,
    val firstName: String,
    val locationState: String,
    val locationStreet: String,
    val locationCity: String,
    val locationTimeZone: String,
    val locationCountry: String,
    val email: String,
    val gender: String,
    val title: String,
    val registeredDate: String,
    val pictureLink: String,
    val dateOfBirth: String
)