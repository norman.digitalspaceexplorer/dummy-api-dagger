package com.example.dummyapidagger.models

import com.example.dummyapidagger.objects.Location

data class UserProfileModel constructor(
    val userID: String,
    val phone: String,
    val lastName: String,
    val firstName: String,
    val location: Location,
    val email: String,
    val gender: String,
    val title: String,
    val registeredDate: String,
    val pictureLink: String,
    val dateOfBirth: String
)