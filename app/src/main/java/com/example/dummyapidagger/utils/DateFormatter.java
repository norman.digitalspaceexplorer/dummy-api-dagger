package com.example.dummyapidagger.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings({ "SimpleDateFormat" })
public class DateFormatter {

    private String dateFormatFrom;
    private String dateFormatTo;
    private String formattedDate;

    public static final String DD_MMM_YYYY_HH_MM_SS = "dd MMM yyyy hh:mm:ss";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-DD HH:MM:SS";
    public static final String DD_MMM_YYYY = "dd MMM yyyy";

    public DateFormatter fromStringFormat(String dateFormat) {
        this.dateFormatFrom = dateFormat;
        return this;
    }

    public DateFormatter toStringFormat(String dateFormat) {
        this.dateFormatTo = dateFormat;
        return this;
    }

    public DateFormatter format(String dateString) throws ParseException {
        String _dateString = dateString.replace("T", " ").replace("Z", "");
        SimpleDateFormat format = new SimpleDateFormat(this.dateFormatFrom);
        Date date = format.parse(_dateString);
        format = new SimpleDateFormat(this.dateFormatTo);
        assert date != null;
        formattedDate = format.format(date);
        return this;
    }

    public String build() {
        return this.formattedDate;
    }

}
