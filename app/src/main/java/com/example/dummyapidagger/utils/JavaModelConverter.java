package com.example.dummyapidagger.utils;

import com.example.dummyapidagger.models.PostsModel;
import com.example.dummyapidagger.models.UserPostsModel;

import java.util.ArrayList;
import java.util.List;

public class JavaModelConverter {

    private final List<PostsModel> tempPostsModel;

    public JavaModelConverter() {
        this.tempPostsModel = new ArrayList<>();
    }

    public JavaModelConverter addModel(List<UserPostsModel> userPostsModels) {
        for (UserPostsModel userPostsModel : userPostsModels) {
            tempPostsModel.add(new PostsModel(
                userPostsModel.getId(),
                userPostsModel.getUserID(),
                userPostsModel.getLastName(),
                userPostsModel.getFirstName(),
                userPostsModel.getEmail(),
                userPostsModel.getTitle(),
                userPostsModel.getPictureLink(),
                userPostsModel.getPostID(),
                userPostsModel.getPostImage(),
                userPostsModel.getPostContent(),
                userPostsModel.getPublishDate(),
                userPostsModel.getLinks(),
                userPostsModel.getLikes()
            ));
        }
        return this;
    }

    public List<PostsModel> build() {
        return this.tempPostsModel;
    }

}
