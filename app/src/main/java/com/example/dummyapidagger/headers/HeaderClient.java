package com.example.dummyapidagger.headers;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.Request;

// YOU CAN ADD HERE YOUR OWN app-id HEADER FROM https://dummyapi.io/
// SIGN UP AND GET A FREE DUMMY API-SERVICE
// 500 CALLS PER DAY
public class HeaderClient {

    @Inject
    OkHttpClient.Builder httpClient;

    @Inject
    public HeaderClient() {
        httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(chain -> {
            Request request = chain.request().newBuilder().addHeader("app-id", "604200a3b40a213ed14fe312").build();
            return chain.proceed(request);
        });
    }

    @Inject
    public OkHttpClient.Builder getHttpClient() {
        return httpClient;
    }

}
