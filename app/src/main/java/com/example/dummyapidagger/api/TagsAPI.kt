package com.example.dummyapidagger.api

import com.example.dummyapidagger.dagger.component.DaggerDomainInjectorComponent
import com.example.dummyapidagger.dagger.modules.APIModule
import com.example.dummyapidagger.objects.Posts
import com.example.dummyapidagger.objects.Tags
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TagsAPI {
    @GET("data/api/tag?limit=30")
    fun getTagsAsync(@Query("page") page: Int) : Deferred<Tags>
    @GET("data/api/tag/{post_tag}/post?limit=10")
    fun getPostsAsync(@Path("post_tag") postTag: String, @Query("page") page: Int) : Deferred<Posts>
}

object NetworkTags {
    private val domain = DaggerDomainInjectorComponent.builder().aPIModule(APIModule()).build()
    val api = domain.getTagsAPI()
}