package com.example.dummyapidagger.api

import com.example.dummyapidagger.dagger.component.DaggerDomainInjectorComponent
import com.example.dummyapidagger.dagger.modules.APIModule
import com.example.dummyapidagger.objects.Users
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersAPI {
    @GET("data/api/user?limit=10")
    fun getUsersAsync(@Query("page") page: Int) : Deferred<Users>
}

object NetworkUsers {
    private val domain = DaggerDomainInjectorComponent.builder().aPIModule(APIModule()).build()
    val api = domain.getUsersAPI()
}