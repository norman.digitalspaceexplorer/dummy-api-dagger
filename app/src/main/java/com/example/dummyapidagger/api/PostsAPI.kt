package com.example.dummyapidagger.api

import com.example.dummyapidagger.dagger.component.DaggerDomainInjectorComponent
import com.example.dummyapidagger.dagger.modules.APIModule
import com.example.dummyapidagger.objects.Posts
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PostsAPI {
    @GET("data/api/post?limit=10")
    fun getPostsAsync(@Query("page") page: Int) : Deferred<Posts>
    @GET("data/api/user/{user_id}/post?limit=10")
    fun getUserPostsAsync(@Path("user_id") userID: String, @Query("page") page: Int) : Deferred<Posts>
}

object NetworkPosts {
    private val domain = DaggerDomainInjectorComponent.builder().aPIModule(APIModule()).build()
    val api = domain.getPostsAPI()
}