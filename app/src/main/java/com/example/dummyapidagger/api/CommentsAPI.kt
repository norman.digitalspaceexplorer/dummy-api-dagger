package com.example.dummyapidagger.api

import com.example.dummyapidagger.dagger.component.DaggerDomainInjectorComponent
import com.example.dummyapidagger.dagger.modules.APIModule
import com.example.dummyapidagger.objects.Comments
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CommentsAPI {
    @GET("data/api/post/{post_id}/comment?limit=100")
    fun getCommentsAsync(@Path("post_id") postID: String, @Query("page") page: Int) : Deferred<Comments>
}

object NetworkComments {
    private val domain = DaggerDomainInjectorComponent.builder().aPIModule(APIModule()).build()
    val api = domain.getCommentsAPI()
}