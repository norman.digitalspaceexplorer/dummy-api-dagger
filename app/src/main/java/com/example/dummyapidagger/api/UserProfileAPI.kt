package com.example.dummyapidagger.api

import com.example.dummyapidagger.dagger.component.DaggerDomainInjectorComponent
import com.example.dummyapidagger.dagger.modules.APIModule
import com.example.dummyapidagger.objects.UserProfile
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path

interface UserProfileAPI {
    @GET("data/api/user/{user_id}")
    fun getUserProfileAsync(@Path("user_id") userID: String) : Deferred<UserProfile>
}

object NetworkUserProfile {
    private val domain = DaggerDomainInjectorComponent.builder().aPIModule(APIModule()).build()
    val api = domain.getUserProfileAPI()
}