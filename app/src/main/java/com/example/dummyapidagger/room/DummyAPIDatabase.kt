package com.example.dummyapidagger.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.dummyapidagger.dao.*
import com.example.dummyapidagger.entities.*

@Database(entities = [
    UsersEntity::class, PostsEntity::class,
    TagsEntity::class, TIDEntity::class,
    ProfileEntity::class, CommentsEntity::class,
    UserProfileEntity::class, UserPostsEntity::class,
    UserTIDEntity::class, TagsPostsEntity::class,
    TagsPostsTIDEntity::class
], version = 8, exportSchema = false)

abstract class DummyAPIDatabase : RoomDatabase() {

    abstract val usersDao: UsersDao
    abstract val postsDao: PostsDao
    abstract val tagsDao: TagsDao
    abstract val tidDao: TIDDao
    abstract val profileDao: ProfileDao
    abstract val commentsDao: CommentsDao
    abstract val userProfileDao: UserProfileDao
    abstract val userPostsDao: UserPostsDao
    abstract val userTIDDao: UserTIDDao
    abstract val tagsPostsDao: TagsPostsDao
    abstract val tagsPostsTIDDao: TagsPostsTIDDao

    companion object {
        @Volatile
        private var INSTANCE: DummyAPIDatabase? = null
        fun getInstance(context: Context) : DummyAPIDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(context.applicationContext,
                        DummyAPIDatabase::class.java, "MARS")
                        .fallbackToDestructiveMigration()
                        .build()
                }
                INSTANCE = instance
                return instance
            }
        }
    }
}