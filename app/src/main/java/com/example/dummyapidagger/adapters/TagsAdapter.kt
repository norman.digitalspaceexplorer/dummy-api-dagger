package com.example.dummyapidagger.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.dummyapidagger.R
import com.example.dummyapidagger.databinding.RecyclerviewTagsBinding
import com.example.dummyapidagger.models.TagsModel

class TagsAdapter(private val onTagClick: OnTagClick) : RecyclerView.Adapter<TagsAdapter.ViewHolder>() {

    var allTags : List<TagsModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val withBinding : RecyclerviewTagsBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), ViewHolder.LAYOUT, parent, false)
        return ViewHolder(binding = withBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.binding.also {
            it.tagsModel = allTags[position]
            it.tagsContent = onTagClick
        }
    }

    override fun getItemCount(): Int {
        return allTags.size
    }

    class ViewHolder(val binding: RecyclerviewTagsBinding) : RecyclerView.ViewHolder(binding.root) {
        companion object {
            @LayoutRes
            val LAYOUT = R.layout.recyclerview_tags
        }
    }

}

class OnTagClick(val block: (TagsModel) -> Unit) {
    fun onTagClick(tagsModel: TagsModel) = block(tagsModel)
}