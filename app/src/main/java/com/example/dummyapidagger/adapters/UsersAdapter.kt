package com.example.dummyapidagger.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.dummyapidagger.R
import com.example.dummyapidagger.databinding.RecyclerviewUsersBinding
import com.example.dummyapidagger.models.UsersModel

class UsersAdapter(private val onUserClick: OnUserClick) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {

    var allUsers: List<UsersModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val withBinding: RecyclerviewUsersBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), ViewHolder.LAYOUT, parent, false)
        return ViewHolder(binding = withBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.binding.also {
            it.usersModel = allUsers[position]
            it.usersContent = onUserClick
        }
        viewHolder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return allUsers.size
    }

    class ViewHolder(val binding: RecyclerviewUsersBinding) : RecyclerView.ViewHolder(binding.root) {
        companion object {
            @LayoutRes
            val LAYOUT = R.layout.recyclerview_users
        }
    }

}

class OnUserClick(val block: (UsersModel) -> Unit) {
    fun onUserClick(usersModel: UsersModel) = block(usersModel)
}
