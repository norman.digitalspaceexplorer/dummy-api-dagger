package com.example.dummyapidagger.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.dummyapidagger.R
import com.example.dummyapidagger.databinding.RecyclerviewPostTagsBinding
import com.example.dummyapidagger.models.TIDModel

class TIDAdapter : RecyclerView.Adapter<TIDAdapter.ViewHolder>() {

    var tagNames: List<TIDModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val withBinding: RecyclerviewPostTagsBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                ViewHolder.LAYOUT, parent, false
        )
        return ViewHolder(binding = withBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.binding.also {
            it.tidModel = tagNames[position]
        }
        viewHolder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return tagNames.size
    }

    class ViewHolder(val binding: RecyclerviewPostTagsBinding) : RecyclerView.ViewHolder(binding.root) {
        companion object {
            @LayoutRes
            val LAYOUT = R.layout.recyclerview_post_tags
        }
    }

}