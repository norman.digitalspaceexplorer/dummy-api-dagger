package com.example.dummyapidagger.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.dummyapidagger.R
import com.example.dummyapidagger.databinding.RecyclerviewCommentsBinding
import com.example.dummyapidagger.models.CommentsModel

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {

    var allComments: List<CommentsModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val withBinding: RecyclerviewCommentsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            ViewHolder.LAYOUT, parent, false
        )
        return ViewHolder(binding = withBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.binding.also {
            it.commentModel = allComments[position]
        }
        viewHolder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return allComments.size
    }

    class ViewHolder(val binding: RecyclerviewCommentsBinding) : RecyclerView.ViewHolder(binding.root) {
        companion object {
            @LayoutRes
            val LAYOUT = R.layout.recyclerview_comments
        }
    }

}