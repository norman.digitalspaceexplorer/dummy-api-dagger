package com.example.dummyapidagger.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.dummyapidagger.R
import com.example.dummyapidagger.databinding.RecyclerviewPostsBinding
import com.example.dummyapidagger.models.PostsModel
import com.example.dummyapidagger.models.TIDModel
import com.example.dummyapidagger.viewmodel.DummyAPIViewModel

class PostsAdapter(
    private val defaultOwner: String, private val onPostsClick: OnPostsClick,
    private val vm: DummyAPIViewModel, private val lifeCycle: LifecycleOwner)
    : RecyclerView.Adapter<PostsAdapter.ViewHolder>() {

    var allPosts: List<PostsModel> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val withBinding: RecyclerviewPostsBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                ViewHolder.LAYOUT, parent, false
        )
        return ViewHolder(binding = withBinding, vm = vm, lc = lifeCycle)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.binding.also {
            it.postsModel = allPosts[position]
            it.postsContent = onPostsClick
        }
        viewHolder.bindTags(postsModel = allPosts[position], owner = defaultOwner)
        viewHolder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return allPosts.size
    }

    class ViewHolder(val binding: RecyclerviewPostsBinding, val vm: DummyAPIViewModel, val lc: LifecycleOwner) : RecyclerView.ViewHolder(binding.root){
        companion object {
            @LayoutRes
            val LAYOUT = R.layout.recyclerview_posts
        }
        fun bindTags(postsModel: PostsModel, owner: String) {
            val tagNames: MutableList<TIDModel> = mutableListOf()
            val tidAdapter = TIDAdapter()
            val layoutManager = LinearLayoutManager(binding.root.context, LinearLayoutManager.HORIZONTAL, false)
            binding.recyclerViewPostTags.layoutManager = layoutManager
            binding.recyclerViewPostTags.adapter = tidAdapter
            this.postsTags(owner = owner).observe(lc, {
                it.map { tid ->
                    if (tid.postID == postsModel.postID) {
                        tagNames.add(TIDModel(id = tid.id, postID = tid.postID, tagNames = tid.tagNames))
                    }
                }
                tidAdapter.tagNames = tagNames
            })
        }

        private fun postsTags(owner: String) : LiveData<List<TIDModel>> {
            return when(owner) {
                "Posts" -> vm.liveData.allPostsTags
                "UserProfile" -> vm.liveData.allUserPostsTags
                else -> vm.liveData.allTagPostsTID
            }
        }
    }

}

class OnPostsClick(val block: (PostsModel) -> Unit) {
    fun onPostsClick(postsModel: PostsModel) = block(postsModel)
}