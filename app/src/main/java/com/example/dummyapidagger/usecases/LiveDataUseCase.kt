package com.example.dummyapidagger.usecases

import androidx.lifecycle.LiveData
import com.example.dummyapidagger.models.*

data class LiveDataUseCase constructor(
    val allUsers: LiveData<List<UsersModel>>,
    val allPosts: LiveData<List<PostsModel>>,
    val allPostsTags: LiveData<List<TIDModel>>,
    val allTags: LiveData<List<TagsModel>>,
    val allUserProfile: LiveData<List<UserProfileModel>>,
    val allUserPosts: LiveData<List<UserPostsModel>>,
    val allUserPostsTags: LiveData<List<TIDModel>>,
    val allPostComments: LiveData<List<CommentsModel>>,
    val allTagPosts: LiveData<List<PostsModel>>,
    val allTagPostsTID: LiveData<List<TIDModel>>
)