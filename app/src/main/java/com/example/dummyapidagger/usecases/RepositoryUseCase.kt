package com.example.dummyapidagger.usecases

import com.example.dummyapidagger.repository.*

data class RepositoryUseCase constructor(
    val user: UsersRepository,
    val posts: PostsRepository,
    val tid: TIDRepository,
    val tags: TagsRepository,
    val profile: UserProfileRepository,
    val comments: CommentsRepository
)