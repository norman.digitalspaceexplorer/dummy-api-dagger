package com.example.dummyapidagger.activities;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.example.dummyapidagger.R;
import com.example.dummyapidagger.adapters.CommentsAdapter;
import com.example.dummyapidagger.databinding.BottomsheetFragmentCommentsBinding;
import com.example.dummyapidagger.utils.NetworkConfig;
import com.example.dummyapidagger.viewmodel.DummyAPIViewModel;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import timber.log.Timber;

public class PostsComments extends BottomSheetDialogFragment {

    private String postID = "";
    private String totalLikes = "";

    BottomsheetFragmentCommentsBinding binding;
    DummyAPIViewModel vm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.bottomsheet_fragment_comments, container, false);
        assert getArguments() != null;
        postID = getArguments().getString("postID");
        totalLikes = getArguments().getString("totalLikes");
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Timber.d("PostCommentsDialog Created ...");

        Application application = Objects.requireNonNull(getActivity()).getApplication();
        vm = new ViewModelProvider(this, new DummyAPIViewModel.Factory(application)).get(DummyAPIViewModel.class);

        CommentsAdapter commentsAdapter = new CommentsAdapter();
        binding.recyclerViewComments.setAdapter(commentsAdapter);

        if(new NetworkConfig(getActivity()).isNetworkAvailable()) {
            vm.deleteAllUserPostsComments();
            vm.getAllDomainPostComments(postID, 0);
        }
        vm.getLiveData().getAllPostComments().observe(getViewLifecycleOwner(), it -> {
            if (it != null && it.size() != 0) {
                commentsAdapter.setAllComments(it);
                binding.textViewTotalComments.setText(String.valueOf(it.size()).concat(it.size() <= 1 ? " Comment" : " Comments"));
                binding.progressCircularLoading.setVisibility(View.GONE);
                return;
            }
            binding.progressCircularLoading.setVisibility(View.VISIBLE);
        });

        binding.textViewLikes.setText(totalLikes.concat(" Likes"));

    }

}
