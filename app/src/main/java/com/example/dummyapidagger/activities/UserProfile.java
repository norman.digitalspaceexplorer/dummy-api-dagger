package com.example.dummyapidagger.activities;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.example.dummyapidagger.R;
import com.example.dummyapidagger.adapters.OnPostsClick;
import com.example.dummyapidagger.adapters.PostsAdapter;
import com.example.dummyapidagger.databinding.ActivityUserProfileBinding;
import com.example.dummyapidagger.interfaces.BottomSheetDialogListener;
import com.example.dummyapidagger.models.PostsModel;
import com.example.dummyapidagger.utils.JavaModelConverter;
import com.example.dummyapidagger.utils.NetworkConfig;
import com.example.dummyapidagger.viewmodel.DummyAPIViewModel;

import kotlin.Unit;

public class UserProfile extends AppCompatActivity implements NestedScrollView.OnScrollChangeListener, BottomSheetDialogListener {

    private String userID;
    private int page = 0;

    ActivityUserProfileBinding binding;
    DummyAPIViewModel vm;

    PostsAdapter postsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_profile);

        userID = getIntent().getStringExtra("userID");
        vm = new ViewModelProvider(this, new DummyAPIViewModel.Factory(getApplication())).get(DummyAPIViewModel.class);

        postsAdapter = new PostsAdapter(
            "UserProfile",
            new OnPostsClick(postsModel -> {
                onFragmentShow(postsModel);
                return Unit.INSTANCE;
            }),
            vm, this);
        binding.recyclerViewPosts.setAdapter(postsAdapter);

        if (new NetworkConfig(this).isNetworkAvailable()) {
            vm.deleteAllUserProfile();
            vm.deleteAllUserPosts();
            vm.deleteAllUserPostsTags();
            vm.getDomainUserProfile(userID);
            vm.getDomainUserPosts(userID, page);
        }

        vm.getLiveData().getAllUserProfile().observe(this, it -> {
            if (it != null && it.size() != 0) {
                binding.setUserProfile(it.get(0));
            }
        });

        vm.getLiveData().getAllUserPosts().observe(this, it -> {
            if (it != null && it.size() != 0) {
                postsAdapter.setAllPosts(new JavaModelConverter().addModel(it).build());
                binding.scrollView.setOnScrollChangeListener(this);
                page = page + 1;
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        finish();
    }

    @Override
    public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (scrollY == (v.getChildAt(0).getMeasuredHeight()-v.getMeasuredHeight())) {
            if (new NetworkConfig(this).isNetworkAvailable()) vm.getDomainUserPosts(userID, page);
            binding.progressCircular.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFragmentShow(PostsModel postsModel) {
        Bundle bundle = new Bundle();
        PostsComments postsComments = new PostsComments();
        bundle.putString("postID", postsModel.getPostID());
        bundle.putString("totalLikes", String.valueOf(postsModel.getLikes()));
        postsComments.setArguments(bundle);
        postsComments.show(getSupportFragmentManager(), "UserProfile");
    }

}
