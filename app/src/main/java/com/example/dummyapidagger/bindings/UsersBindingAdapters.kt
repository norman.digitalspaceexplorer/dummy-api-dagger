package com.example.dummyapidagger.bindings

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.dummyapidagger.models.UsersModel
import com.squareup.picasso.Picasso
import java.util.*

@BindingAdapter("imagePath")
fun setUsersImage(imageView: ImageView, imageURL: String?) {
    Picasso.get().load(imageURL).into(imageView)
}

@BindingAdapter("fullName")
fun setFullName(textView: TextView, usersModel: UsersModel) {
    val titleCaps: String = usersModel.title.capitalize(Locale.ROOT)
    val fullName = "$titleCaps. ${usersModel.firstName} ${usersModel.lastName}"
    textView.text = fullName
}

@BindingAdapter("userID")
fun setUserID(textView: TextView, usersModel: UsersModel) {
    val userID = "User ID: ${usersModel.userID}"
    textView.text = userID
}


