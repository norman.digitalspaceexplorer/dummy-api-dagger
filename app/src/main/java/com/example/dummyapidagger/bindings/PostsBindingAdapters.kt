package com.example.dummyapidagger.bindings

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.dummyapidagger.models.PostsModel
import com.example.dummyapidagger.utils.DateFormatter
import java.util.*

@BindingAdapter("fullNamePosts")
fun setPostsFullName(textView: TextView, postsModel: PostsModel) {
    val titleCaps: String = postsModel.title.capitalize(Locale.ROOT)
    val fullName = "$titleCaps. ${postsModel.firstName} ${postsModel.lastName}"
    textView.text = fullName
}

@BindingAdapter("postLikes")
fun setLikes(textView: TextView, postsModel: PostsModel) {
    val likes = "${postsModel.likes} Likes"
    textView.text = likes
}

@BindingAdapter("linksVisible")
fun setLinkVisibility(textView: TextView, link: String?) {
    if (link == null || link == "") textView.visibility = View.GONE
    else textView.visibility = View.VISIBLE
}

@BindingAdapter("formatPublishedDate")
fun formatPublishedDate(textView: TextView, dateToFormat: String?) {
    dateToFormat?.let {
        val format = DateFormatter()
            .fromStringFormat(DateFormatter.YYYY_MM_DD_HH_MM_SS)
            .toStringFormat(DateFormatter.DD_MMM_YYYY_HH_MM_SS)
            .format(dateToFormat)
            .build()
        textView.text = format
    }
}