package com.example.dummyapidagger.bindings

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.dummyapidagger.models.UserProfileModel
import com.example.dummyapidagger.utils.DateFormatter
import java.util.*

@BindingAdapter("fullNameUserProfile")
fun setFullName(textView: TextView, userProfileModel: UserProfileModel?) {
    userProfileModel?.let {
        val titleCaps: String = userProfileModel.title.capitalize(Locale.ROOT)
        val fullName = "$titleCaps. ${userProfileModel.firstName} ${userProfileModel.lastName}"
        textView.text = fullName
    }
}

@BindingAdapter("userProfileAddress")
fun setUserAddress(textView: TextView, userProfileModel: UserProfileModel?) {
    userProfileModel?.let {
        val address = "${userProfileModel.location.street}, ${userProfileModel.location.city} " +
                "${userProfileModel.location.state}, ${userProfileModel.location.country}, ${userProfileModel.location.timezone}"
        textView.text = address
    }
}

@BindingAdapter("formatBirthDate")
fun formatBirthDate(textView: TextView, dateToFormat: String?) {
    dateToFormat?.let {
        val format = DateFormatter()
            .fromStringFormat(DateFormatter.YYYY_MM_DD_HH_MM_SS)
            .toStringFormat(DateFormatter.DD_MMM_YYYY)
            .format(dateToFormat)
            .build()
        textView.text = format
    }
}

@BindingAdapter("formatRegisteredDate")
fun formatRegisteredDate(textView: TextView, dateToFormat: String?) {
    dateToFormat?.let {
        val format = DateFormatter()
            .fromStringFormat(DateFormatter.YYYY_MM_DD_HH_MM_SS)
            .toStringFormat(DateFormatter.DD_MMM_YYYY)
            .format(dateToFormat)
            .build()
        textView.text = format
    }
}