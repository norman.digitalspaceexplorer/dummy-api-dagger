package com.example.dummyapidagger.bindings

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.dummyapidagger.models.CommentsModel
import java.util.*

@BindingAdapter("fullNameComments")
fun setFullName(textView: TextView, commentsModel: CommentsModel?) {
    commentsModel?.let {
        val titleCaps: String = commentsModel.title.capitalize(Locale.ROOT)
        val fullName = "$titleCaps. ${commentsModel.firstName} ${commentsModel.lastName}"
        textView.text = fullName
    }
}