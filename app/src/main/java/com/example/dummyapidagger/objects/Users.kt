package com.example.dummyapidagger.objects

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Users constructor(
    val data: List<UsersData>
)

data class UsersData constructor(
    val id: String,
    val email: String,
    val title: String,
    val firstName: String,
    val lastName: String,
    val picture: String
)