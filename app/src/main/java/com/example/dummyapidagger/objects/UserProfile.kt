package com.example.dummyapidagger.objects

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserProfile constructor(
    @Json(name = "id")
    val userID: String,
    val phone: String,
    val lastName: String,
    val firstName: String,
    val location: Location,
    val email: String,
    val gender: String,
    val title: String,
    @Json(name = "registerDate")
    val registeredDate: String,
    @Json(name = "picture")
    val pictureLink: String,
    val dateOfBirth: String
)

data class Location constructor(
    val state: String,
    val street: String,
    val city: String,
    val timezone: String,
    val country: String
)