package com.example.dummyapidagger.objects

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Posts constructor(
    val data: List<PostsData>
)

@JsonClass(generateAdapter = true)
data class PostsData constructor(
    val owner: UsersData,
    @Json(name = "id")
    val postID: String,
    @Json(name = "image")
    val postImage: String,
    @Json(name = "publishDate")
    val postPublishDate: String,
    val text: String,
    val tags: List<String>,
    val link: String?,
    val likes: Int
)