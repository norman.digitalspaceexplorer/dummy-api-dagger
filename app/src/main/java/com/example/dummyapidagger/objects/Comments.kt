package com.example.dummyapidagger.objects

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Comments constructor(
    val data: List<CommentsContent>
)

data class CommentsContent constructor(
    val owner: UsersData,
    val id: String,
    val message: String,
    val publishDate: String
)