package com.example.dummyapidagger.objects

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Tags constructor(
    @Json(name = "data")
    val data: List<String>
)