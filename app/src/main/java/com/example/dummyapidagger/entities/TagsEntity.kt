package com.example.dummyapidagger.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "VEN_TBL_TAGS")
data class TagsEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val tags: String
)