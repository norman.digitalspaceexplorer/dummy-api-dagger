package com.example.dummyapidagger.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "VEN_TBL_USERS")
data class UsersEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "user_id")
    val userID: String,
    @ColumnInfo(name = "last_name")
    val lastName: String,
    @ColumnInfo(name = "first_name")
    val firstName: String,
    @ColumnInfo(name = "email_address")
    val email: String,
    val title: String,
    @ColumnInfo(name = "picture_link")
    val pictureLink: String
)