package com.example.dummyapidagger.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "VEN_TBL_USER_TID")
data class UserTIDEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "post_id")
    val postID: String,
    @ColumnInfo(name = "tag_names")
    val tagNames: String
)