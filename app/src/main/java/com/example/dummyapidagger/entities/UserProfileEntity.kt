package com.example.dummyapidagger.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "VEN_TBL_USER_PROFILE")
data class UserProfileEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "user_id")
    val userID: String,
    val phone: String,
    @ColumnInfo(name = "last_name")
    val lastName: String,
    @ColumnInfo(name = "first_name")
    val firstName: String,
    @ColumnInfo(name = "location_state")
    val locationState: String,
    @ColumnInfo(name = "location_street")
    val locationStreet: String,
    @ColumnInfo(name = "location_city")
    val locationCity: String,
    @ColumnInfo(name = "location_timezone")
    val locationTimeZone: String,
    @ColumnInfo(name = "location_country")
    val locationCountry: String,
    val email: String,
    val gender: String,
    val title: String,
    @ColumnInfo(name = "registered_date")
    val registeredDate: String,
    @ColumnInfo(name = "picture_link")
    val pictureLink: String,
    @ColumnInfo(name = "date_of_birth")
    val dateOfBirth: String
)