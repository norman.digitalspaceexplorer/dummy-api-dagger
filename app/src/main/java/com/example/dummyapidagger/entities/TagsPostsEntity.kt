package com.example.dummyapidagger.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "VEN_TBL_TAGS_POSTS")
data class TagsPostsEntity constructor(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @ColumnInfo(name = "user_id")
    val userID: String,
    @ColumnInfo(name = "last_name")
    val lastName: String,
    @ColumnInfo(name = "first_name")
    val firstName: String,
    @ColumnInfo(name = "email_address")
    val email: String,
    val title: String,
    @ColumnInfo(name = "picture_link")
    val pictureLink: String,
    @ColumnInfo(name = "post_id")
    val postID: String,
    @ColumnInfo(name = "post_image")
    val postImage: String,
    @ColumnInfo(name = "post_content")
    val postContent: String,
    @ColumnInfo(name = "publish_date")
    val publishDate: String,
    val links: String?,
    val likes: Int
)
