package com.example.dummyapidagger.interfaces;

import com.example.dummyapidagger.models.PostsModel;

public interface BottomSheetDialogListener {

    void onFragmentShow(PostsModel postsModel);

}
