package com.example.dummyapidagger.dagger.modules

import com.example.dummyapidagger.dagger.scopes.DomainInjectorScope
import com.example.dummyapidagger.headers.HeaderClient
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
class RetrofitModule {

    @DomainInjectorScope
    @Provides
    fun providesRetrofit(moshI: Moshi, coroutineCallAdapterFactory: CoroutineCallAdapterFactory) : Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://dummyapi.io/")
            .addConverterFactory(MoshiConverterFactory.create(moshI))
            .addCallAdapterFactory(coroutineCallAdapterFactory)
            .client(HeaderClient().httpClient.build())
            .build()
    }

    @Provides
    fun providesMoshIBuilder(kotlinJsonAdapterFactory: KotlinJsonAdapterFactory) : Moshi {
        return Moshi.Builder().add(kotlinJsonAdapterFactory).build()
    }

    @Provides
    fun providesKotlinJSON() : KotlinJsonAdapterFactory {
        return KotlinJsonAdapterFactory()
    }

    @Provides
    fun providesCoroutineAdapter() : CoroutineCallAdapterFactory {
        return CoroutineCallAdapterFactory()
    }

}