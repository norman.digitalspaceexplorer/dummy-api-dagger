package com.example.dummyapidagger.dagger.component

import com.example.dummyapidagger.api.*
import com.example.dummyapidagger.dagger.modules.APIModule
import com.example.dummyapidagger.dagger.modules.RetrofitModule
import com.example.dummyapidagger.dagger.scopes.DomainInjectorScope
import dagger.Component

@DomainInjectorScope
@Component(modules = [ RetrofitModule::class, APIModule::class ])
interface DomainInjectorComponent {

    fun getUsersAPI() : UsersAPI

    fun getPostsAPI() : PostsAPI

    fun getTagsAPI() : TagsAPI

    fun getUserProfileAPI() : UserProfileAPI

    fun getCommentsAPI() : CommentsAPI

}