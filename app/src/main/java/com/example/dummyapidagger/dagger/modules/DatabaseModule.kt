package com.example.dummyapidagger.dagger.modules

import android.app.Application
import com.example.dummyapidagger.dagger.scopes.ViewModelScope
import com.example.dummyapidagger.room.DummyAPIDatabase
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule constructor(private val application: Application) {

    @ViewModelScope
    @Provides
    fun providesDatabase() : DummyAPIDatabase {
        return DummyAPIDatabase.getInstance(application.applicationContext)
    }

}