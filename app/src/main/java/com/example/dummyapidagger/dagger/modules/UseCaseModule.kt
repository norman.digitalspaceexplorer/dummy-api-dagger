package com.example.dummyapidagger.dagger.modules

import com.example.dummyapidagger.repository.*
import com.example.dummyapidagger.room.DummyAPIDatabase
import com.example.dummyapidagger.usecases.LiveDataUseCase
import com.example.dummyapidagger.usecases.RepositoryUseCase
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    @Provides
    fun providesRepositoryUseCase(dummyAPIDatabase: DummyAPIDatabase) : RepositoryUseCase {
        return RepositoryUseCase(
            user = UsersRepository(dummyAPIDatabase = dummyAPIDatabase),
            posts = PostsRepository(dummyAPIDatabase = dummyAPIDatabase),
            tid = TIDRepository(dummyAPIDatabase = dummyAPIDatabase),
            tags = TagsRepository(dummyAPIDatabase = dummyAPIDatabase),
            profile = UserProfileRepository(dummyAPIDatabase = dummyAPIDatabase),
            comments = CommentsRepository(dummyAPIDatabase = dummyAPIDatabase)
        )
    }

    @Provides
    fun providesLiveDataUseCase(repository: RepositoryUseCase) : LiveDataUseCase {
        return LiveDataUseCase(
            allUsers = repository.user.allUsers,
            allPosts = repository.posts.allPosts,
            allPostsTags = repository.tid.allPostTags,
            allTags = repository.tags.allTags,
            allUserProfile = repository.profile.allUserProfile,
            allUserPosts = repository.profile.allUserPosts,
            allUserPostsTags = repository.profile.allUserPostsTID,
            allPostComments = repository.comments.allPostComments,
            allTagPosts = repository.tags.allTagsPosts,
            allTagPostsTID = repository.tags.allTagsPostsTID
        )
    }

}