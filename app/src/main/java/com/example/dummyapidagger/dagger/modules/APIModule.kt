package com.example.dummyapidagger.dagger.modules

import com.example.dummyapidagger.api.*
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class APIModule {

    @Provides
    fun providesUsersAPI(retrofit: Retrofit) : UsersAPI {
        return retrofit.create(UsersAPI::class.java)
    }

    @Provides
    fun providesPostsAPI(retrofit: Retrofit) : PostsAPI {
        return retrofit.create(PostsAPI::class.java)
    }

    @Provides
    fun providesTagsAPI(retrofit: Retrofit) : TagsAPI {
        return retrofit.create(TagsAPI::class.java)
    }

    @Provides
    fun providesUserProfileAPI(retrofit: Retrofit) : UserProfileAPI {
        return retrofit.create(UserProfileAPI::class.java)
    }

    @Provides
    fun providesCommentsAPI(retrofit: Retrofit) : CommentsAPI {
        return retrofit.create(CommentsAPI::class.java)
    }

}