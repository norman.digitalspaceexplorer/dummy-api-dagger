package com.example.dummyapidagger.dagger.component

import com.example.dummyapidagger.dagger.modules.DatabaseModule
import com.example.dummyapidagger.dagger.modules.UseCaseModule
import com.example.dummyapidagger.dagger.scopes.ViewModelScope
import com.example.dummyapidagger.viewmodel.DummyAPIViewModel
import dagger.Component

@ViewModelScope
@Component(modules = [ DatabaseModule::class, UseCaseModule::class ])
interface ViewModelComponent {

    fun inject(viewModel: DummyAPIViewModel)

}