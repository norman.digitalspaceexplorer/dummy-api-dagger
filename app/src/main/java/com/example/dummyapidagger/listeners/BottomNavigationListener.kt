package com.example.dummyapidagger.listeners

import android.app.Activity
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.dummyapidagger.R
import com.example.dummyapidagger.fragments.PostsFragment
import com.example.dummyapidagger.fragments.TagsFragment
import com.example.dummyapidagger.fragments.UsersFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class BottomNavigationListener(private val activity: Activity, private val fragmentManager: FragmentManager) : BottomNavigationView.OnNavigationItemSelectedListener {

    private var activeFragment: String = ""

    private var usersListFragment: Fragment? = null
    private var postsListFragment: Fragment? = null
    private var tagsListFragment: Fragment? = null
    private var selectedFragment: Fragment? = null

    init {
        activeFragment = activity.getString(R.string.users_list)
        usersListFragment = UsersFragment()
        selectedFragment = usersListFragment
        fragmentManager.beginTransaction().add(R.id.fragment_container, selectedFragment!!).commit()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.bottomNavUsersList -> {
                if (activeFragment != activity.getString(R.string.users_list)) {
                    if (usersListFragment == null) {
                        usersListFragment = UsersFragment()
                        fragmentManager.beginTransaction().add(R.id.fragment_container, usersListFragment!!).commit()
                    }
                    fragmentManager.beginTransaction().hide(selectedFragment!!).show(usersListFragment!!).commit()
                    selectedFragment = usersListFragment
                    activeFragment = activity.getString(R.string.users_list)
                    activity.title = activeFragment
                }
                return true
            }
            R.id.bottomNavPostsList -> {
                if (activeFragment != activity.getString(R.string.posts_list)) {
                    if (postsListFragment == null) {
                        postsListFragment = PostsFragment()
                        fragmentManager.beginTransaction().add(R.id.fragment_container, postsListFragment!!).commit()
                    }
                    fragmentManager.beginTransaction().hide(selectedFragment!!).show(postsListFragment!!).commit()
                    selectedFragment = postsListFragment
                    activeFragment = activity.getString(R.string.posts_list)
                    activity.title = activeFragment
                }
                return true
            }
            R.id.bottomNavTagsList -> {
                if (activeFragment != activity.getString(R.string.tags_list)) {
                    if (tagsListFragment == null) {
                        tagsListFragment = TagsFragment()
                        fragmentManager.beginTransaction().add(R.id.fragment_container, tagsListFragment!!).commit()
                    }
                    fragmentManager.beginTransaction().hide(selectedFragment!!).show(tagsListFragment!!).commit()
                    selectedFragment = tagsListFragment
                    activeFragment = activity.getString(R.string.tags_list)
                    activity.title = activeFragment
                }
                return true
            }
            else -> true
        }
    }
}